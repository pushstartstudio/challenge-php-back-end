# Desafio de Programação #

O desafio é desenvolver uma API básica em PHP e MySQL. A API deve permitir que um usuário faça login, e após logado: consulte e edite seu perfil e faça logout.

## Tecnologias ##

Deve ser implementado usando PHP, base de dados MySQL, Apache e comunicação em JSON.

Procure fazer com PHP puro. Caso deseje usar um framework, pode-se usar apenas o Slim 3. (não use Laravel)

A sessão deve ser persistida da maneira que desejar. (cookie, JWT etc)

## Recursos ##

A base de dados deve ter no mínimo uma tabela onde os usuários do sistema devem ser armazenados.

A API deve ter endpoints para atender as seguintes necessidades:

1. Logar usando POST: enviar email de acesso e senha. Esté o único endpoint acessível sem estar logado;
2. Consultar perfil com GET: retorna nome, email e URL da imagem do usuário logado;
3. Editar perfil com PUT: altera nome do usuário, email de acesso e senha;
4. Alterar imagem de perfil com POST: altera a imagem do usuário; (dica: content-type não é JSON)
5. Logout com DELETE: invalida a sessão atual.

Todas as alterações devem ser persistidas no banco de dados.

A imagem do usuário pode deve ser acessível pelo browser e tem que ser armazenado em um diretório diferente dos arquivos PHP.

Se não estiver logado e chamar os endpoints de 2-5, a API deve responder que não está logado.

## Dicas ##

* Use XAMPP ou EasyPHP;
* Use o MySQL Workbench ou PHPMyAdmin;
* Sugerimos que aplique REST na API para facilitar o desenvolvimento e testes;
* Crie um HTML para testar a API OU use ferramentas para teste de APIs como por exemplo Insomnia, Postman etc.

## Entrega ##

Para a entrega, deve ser criado um repositório no GitHub ou BitBucket com o projeto seguindo a estrutura base abaixo:

* /src - código fonte da API;
* /db - dump do MySQL para criação da banco de dados e arquivos relacionados se existente (ex.: modelo da base);
* README.md com detalhes de como testar e lista das bibliotecas e frameworks caso tenha usado.

Caso tenha feito um HTML para testes ou usado uma ferramenta de teste de API, informe no arquivo README e inclua na entrega o HTML.
